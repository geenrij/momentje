package momentjev1

import (
	"fmt"
	"hash/crc64"
	"os"
	"time"

	"github.com/dgryski/go-jump"
)

const v1HashDateFormat = "2006-01-02"

var crcTable = crc64.MakeTable(crc64.ECMA)

const slotDuration = 5 * time.Minute

var slots []time.Time

var locAmsterdam *time.Location

func init() {
	var err error
	locAmsterdam, err = time.LoadLocation("Europe/Amsterdam")
	if err != nil {
		fmt.Printf("error loading location: %v\n", err)
		os.Exit(1)
	}
	morning, err := time.ParseInLocation("2006-01-02 15:04:00", "2000-01-01 09:00:00", locAmsterdam)
	if err != nil {
		fmt.Printf("error parsing morning time: %v\n", err)
		os.Exit(1)
	}
	evening, err := time.ParseInLocation("2006-01-02 15:04:00", "2000-01-01 20:00:00", locAmsterdam)
	if err != nil {
		fmt.Printf("error parsing evening time: %v\n", err)
		os.Exit(1)
	}

	for t := morning; t.Before(evening); t = t.Add(slotDuration) {
		slots = append(slots, t)
	}
}

func Hash(date time.Time, zipcode, housenumber string) time.Time {
	data := make([]byte, len(v1HashDateFormat)+len(zipcode)+len(housenumber))
	data = append(data, []byte(zipcode)...)
	data = append(data, []byte(date.Format(v1HashDateFormat))...)
	data = append(data, []byte(housenumber)...)

	key := crc64.Checksum(data, crcTable)
	bucket := jump.Hash(key, len(slots))

	slot := slots[int(bucket)]
	year, month, day := date.Date()
	hour, minute := slot.Hour(), slot.Minute()
	return time.Date(year, month, day, hour, minute, 0, 0, locAmsterdam)
}
