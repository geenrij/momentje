package momentjev1

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

type getMomentRequest struct {
	Zipcode     string `json:"zipcode"`
	Housenumber string `json:"housenumber"`
}

type getMomentResponse struct {
	Moments []getMomentResponseMoment `json:"moments"`
}
type getMomentResponseMoment struct {
	Datetime time.Time `json:"datetime"`
}

func GetMoment(w http.ResponseWriter, r *http.Request) {
	if r.Method == "OPTIONS" {
		// TODO: cors
		return
	}

	var requestData getMomentRequest
	err := json.NewDecoder(r.Body).Decode(&requestData)
	r.Body.Close()
	if err != nil {
		http.Error(w, "failed to decode json", http.StatusBadRequest)
		return
	}
	now := time.Now()
	tomorrow := now.Add(24 * time.Hour)
	var responseData = getMomentResponse{
		Moments: []getMomentResponseMoment{
			getMomentResponseMoment{Datetime: Hash(now, requestData.Zipcode, requestData.Housenumber)},
			getMomentResponseMoment{Datetime: Hash(tomorrow, requestData.Zipcode, requestData.Housenumber)},
		},
	}

	err = json.NewEncoder(w).Encode(&responseData)
	if err != nil {
		http.Error(w, "failed to encode json", http.StatusInternalServerError)
		fmt.Printf("error encoding response: %v", err)
		return
	}
}
