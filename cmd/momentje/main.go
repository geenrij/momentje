package main

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/geenrij/momentje/momentjev2"
	"gitlab.com/geenrij/momentje/openingstijden"
)

func main() {
	openingstijdenUsername, ok := os.LookupEnv("OT_USER")
	if !ok {
		fmt.Println("Missing env var OT_USER")
		os.Exit(1)
	}
	openingstijdenPassword, ok := os.LookupEnv("OT_PASS")
	if !ok {
		fmt.Println("Missing env var OT_PASS")
		os.Exit(1)
	}
	openingstijdenClient, err := openingstijden.NewClient(openingstijdenUsername, openingstijdenPassword)
	if err != nil {
		fmt.Printf("Failed to create openingstijden client: %v", err)
		os.Exit(1)
	}
	api := momentjev2.NewAPI(openingstijdenClient)
	// http.HandleFunc("/v1/get-moment", momentjev1.GetMoment)
	http.HandleFunc("/v2/places", api.Places)
	http.HandleFunc("/v2/moment", api.Moment)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	err = http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
	if err != nil {
		fmt.Printf("fatal: %v\n", err)
		os.Exit(1)
	}
}
