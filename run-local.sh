#!/bin/bash

set -euxo pipefail

go build -o dist/bin/momentje ./cmd/momentje;

./dist/bin/momentje;
