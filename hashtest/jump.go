package main

import (
	"fmt"
	"hash/crc64"
	"strconv"

	"github.com/dgryski/go-jump"
)

const numBuckets = 1024

var table = crc64.MakeTable(crc64.ECMA)
var counters [numBuckets]uint64

func main() {
	for i := 0; i < 9999; i++ {
		for a := 'A'; a <= 'Z'; a++ {
			for b := 'A'; b <= 'Z'; b++ {
				// format zipcode
				address := fmt.Sprintf("%d%c%c", i, a, b)
				for h := uint64(1); h < 160; h++ {
					// add housenumber
					address += strconv.FormatUint(h, 10)
					data := []byte(address)
					key := crc64.Checksum(data, table)
					bucket := jump.Hash(key, numBuckets)
					counters[bucket]++
				}
			}
		}
		if i%10 == 0 {
			var highest uint64
			var lowest uint64 = 18446744073709551615
			for k, v := range counters {
				if v < lowest {
					lowest = v
				}
				if v > highest {
					highest = v
				}
				fmt.Printf("%d:%d\n", k, v)
			}
			diff := highest - lowest
			fmt.Printf("%d to %d\tdiff:%d (%d%%)\n\n ", lowest, highest, diff, diff*100/lowest)
		}
	}
}
