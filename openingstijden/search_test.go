package openingstijden

import (
	"os"
	"testing"

	"github.com/davecgh/go-spew/spew"
)

func TestSearch(t *testing.T) {
	username, ok := os.LookupEnv("OT_USER")
	if !ok {
		t.Fatal("Missing env var OT_USER, these test can only be run with a valid openingstijden API username and password. Sorry :(")
	}
	password, ok := os.LookupEnv("OT_PASS")
	if !ok {
		t.Fatal("Missing env var OT_PASS, these test can only be run with a valid openingstijden API username and password. Sorry :(")
	}

	client, err := NewClient(username, password)
	if err != nil {
		t.Fatalf("Failed to setup client: %v", err)
	}

	searchResult, err := client.Search(52.626456, 4.755706)
	if err != nil {
		t.Fatalf("Failed to search: %v", err)
	}

	spew.Dump(searchResult)
}
