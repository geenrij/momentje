package openingstijden

import (
	"encoding/json"
	"fmt"
	"sort"
	"time"
)

type OpeningTimesList []OpeningTimes

type OpeningTimes struct {
	StartTime time.Time
	EndTime   time.Time
}

type unmarshallOpeningTimes map[string][]string

func (o *OpeningTimesList) UnmarshalJSON(data []byte) error {
	var unmarshalled unmarshallOpeningTimes
	err := json.Unmarshal(data, &unmarshalled)
	if err != nil {
		return fmt.Errorf("failed to unmarshall opening times: %w", err)
	}

	const datetimeFormat = `2006-01-02 1504`
	for date, times := range unmarshalled {
		if len(times) == 0 {
			continue
		}
		if len(times) != 2 {
			return fmt.Errorf("failed to unmarshal openingtimes: expected 2 times for each date, got %d", len(times))
		}

		// TODO: We assume all shops are in the Europe/Amsterdam timezone, which
		// may not be the case. We can corectly parse times if we know the
		// timezone of the shop.

		startTime, err := time.ParseInLocation(datetimeFormat, date+" "+times[0], locationAmsterdam)
		if err != nil {
			return fmt.Errorf("failed to parse time: %w", err)
		}
		endTime, err := time.ParseInLocation(datetimeFormat, date+" "+times[1], locationAmsterdam)
		if err != nil {
			return fmt.Errorf("failed to parse time: %w", err)
		}
		*o = append(*o, OpeningTimes{
			StartTime: startTime,
			EndTime:   endTime,
		})
	}

	// because the opening times are given as an object, keyed by the date
	// (ymd), we need to unmarshall using a map, which are unordered. Sort the
	// openingstimes again here.
	sort.Sort(*o)

	return nil
}

// Len is part of sort.Interface.
func (o OpeningTimesList) Len() int {
	return len(o)
}

// Swap is part of sort.Interface.
func (o OpeningTimesList) Swap(i, j int) {
	o[i], o[j] = o[j], o[i]
}

// Less is part of sort.Interface.
func (o OpeningTimesList) Less(i, j int) bool {
	return o[i].StartTime.Before(o[j].StartTime)
}
