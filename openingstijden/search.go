package openingstijden

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type PlaceSearchResult struct {
	ID      int64   `json:"id"`
	Name    string  `json:"name"`
	Street  string  `json:"street"`
	ZipCode string  `json:"zip_code"`
	City    string  `json:"city"`
	Lat     float64 `json:"lat"`
	Lon     float64 `json:"lng"`
	URL     string  `json:"url"`
}

// https://api.openingstijden.nl/open/v1/search/52.626456,4.755706

// Search returns a list of nearby stores for for a given lat/lon.
func (c *Client) Search(lat, lon float64) ([]PlaceSearchResult, error) {
	searchURL := fmt.Sprintf("https://api.openingstijden.nl/open/v1/search/%f,%f", lat, lon)

	searchRequest, err := http.NewRequest("GET", searchURL, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create search request: %w", err)
	}
	searchRequest.SetBasicAuth(c.username, c.password)

	res, err := c.httpClient.Do(searchRequest)
	if err != nil {
		return nil, fmt.Errorf("failed to get places (search): %w", err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("failed to get places (search): %s", res.Status)
	}
	var places = []PlaceSearchResult{}
	err = json.NewDecoder(res.Body).Decode(&places)
	if err != nil {
		return nil, fmt.Errorf("failed to decode place search result: %w", err)
	}

	return places, nil
}
