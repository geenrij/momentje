package openingstijden

import "net/http"

type Client struct {
	username   string
	password   string
	httpClient *http.Client
}

func NewClient(username, password string) (*Client, error) {
	return &Client{
		username:   username,
		password:   password,
		httpClient: &http.Client{},
	}, nil
}
