package openingstijden

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"
)

var locationAmsterdam *time.Location

func init() {
	var err error
	locationAmsterdam, err = time.LoadLocation("Europe/Amsterdam")
	if err != nil {
		fmt.Printf("Failed to load location Europe/Amsterdam: %v\n", err)
		os.Exit(1)
	}
}

type PlaceDetails struct {
	ID           int64            `json:"id"`
	BusinessID   string           `json:"business_id"`
	BusinessName string           `json:"business_name"`
	CategoryID   string           `json:"category_id"`
	Name         string           `json:"name"`
	Street       string           `json:"street"`
	ZipCode      string           `json:"zip_code"`
	Phone        interface{}      `json:"phone"` // TODO
	City         string           `json:"city"`
	Lat          float64          `json:"lat"`
	Lng          float64          `json:"lng"`
	URL          string           `json:"url"`
	Times        OpeningTimesList `json:"times"`
}

// https://api.openingstijden.nl/open/v1/details/1187271

// Details returns the PlaceDetails for a given Place ID.
func (c *Client) Details(id int64) (*PlaceDetails, error) {
	detailsURL := fmt.Sprintf("https://api.openingstijden.nl/open/v1/details/%d", id)

	detailsRequest, err := http.NewRequest("GET", detailsURL, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create details request: %w", err)
	}
	detailsRequest.SetBasicAuth(c.username, c.password)

	res, err := c.httpClient.Do(detailsRequest)
	if err != nil {
		return nil, fmt.Errorf("failed to get place details: %w", err)
	}

	var details = &PlaceDetails{}
	err = json.NewDecoder(res.Body).Decode(details)
	defer res.Body.Close()
	if err != nil {
		return nil, fmt.Errorf("failed to decode place details: %w", err)
	}

	return details, nil
}
