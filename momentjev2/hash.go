package momentjev2

import (
	"hash/crc64"
	"strings"
	"time"

	"github.com/dgryski/go-jump"
)

const v1HashDateFormat = "2006-01-02"

var crcTable = crc64.MakeTable(crc64.ECMA)

const slotDuration = 5 * time.Minute

func Hash(startTime time.Time, endTime time.Time, zipcode, housenumber string) time.Time {
	zipcode = strings.ToUpper(zipcode)
	housenumber = strings.ToUpper(strings.ReplaceAll(housenumber, " ", ""))
	data := make([]byte, 0, len(v1HashDateFormat)+len(zipcode)+len(housenumber))
	data = append(data, []byte(startTime.Format(v1HashDateFormat))...)
	data = append(data, []byte(zipcode)...)
	data = append(data, []byte(housenumber)...)

	numSlots := int(endTime.Sub(startTime) / slotDuration)

	key := crc64.Checksum(data, crcTable)
	selectedSlot := jump.Hash(key, numSlots)

	slot := startTime.Add(time.Duration(selectedSlot) * slotDuration)
	return slot
}
