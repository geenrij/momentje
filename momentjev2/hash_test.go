package momentjev2

import (
	"fmt"
	"strconv"
	"testing"
	"time"

	"github.com/davecgh/go-spew/spew"
)

func TestHash(t *testing.T) {
	ams, _ := time.LoadLocation("Europe/Amsterdam")
	startTime := time.Date(2020, 04, 14, 8, 00, 00, 00, ams)
	endTime := time.Date(2020, 04, 14, 22, 00, 00, 00, ams)
	times := make(map[time.Time]int)
	for p := 1000; p < 6000; p++ {
		for i := 0; i < 100; i++ {
			t := Hash(startTime, endTime, fmt.Sprintf("$%dZZ", p), strconv.FormatUint(uint64(i), 10))
			times[t] = times[t] + 1
		}
	}
	spew.Dump(times)
}
