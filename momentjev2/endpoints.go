package momentjev2

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/geenrij/momentje/pdok"
)

// We don't want people to find out the shop is going to close in 5 minutes. So
// the last moment we'll present is always 30 minutes before closing time.
const safeShoppingDuration = -30 * time.Minute

type PlacesRequest struct {
	Zipcode     string `json:"zipcode"`
	Housenumber string `json:"housenumber"`
	Distance    uint64 `json:"distance"`
}

type PlacesResponse struct {
	Error           string                 `json:"error"`
	CenterLatitude  float64                `json:"center_latitude"`
	CenterLongitude float64                `json:"center_longitude"`
	Places          []PlacesResponse_Place `json:"places"`
}
type PlacesResponse_Place struct {
	ID         string  `json:"id"`
	Name       string  `json:"name"`
	Streetname string  `json:"streetname"`
	Latitude   float64 `json:"latitude"`
	Longtitude float64 `json:"longitude"`
}

func (a *API) Places(w http.ResponseWriter, r *http.Request) {
	setCors(w)
	if r.Method == "OPTIONS" {
		return
	}

	var requestData PlacesRequest
	err := json.NewDecoder(r.Body).Decode(&requestData)
	r.Body.Close()
	if err != nil {
		http.Error(w, "failed to decode json", http.StatusBadRequest)
		return
	}
	var responseData PlacesResponse
	{
		if len(requestData.Housenumber) > 10 {
			responseData.Error = "Ongeldig huisnummer."
			goto sendResponse
		}
		if len(requestData.Zipcode) > 10 {
			responseData.Error = "Ongeldige postcode."
			goto sendResponse
		}
		zipcode := strings.ReplaceAll(requestData.Zipcode, " ", "")
		if len(zipcode) != 6 {
			responseData.Error = "Ongeldige postcode."
			goto sendResponse
		}

		locationSuggest, err := pdok.SuggestZipcodeHousenumber(zipcode, requestData.Housenumber)
		if err != nil {
			fmt.Printf("error getting centerpoint suggestion: %v\n", err)
			http.Error(w, "failed to get centerpoint suggestion", http.StatusInternalServerError)
			return
		}
		if locationSuggest.Response.NumFound == 0 {
			responseData.Error = "Postcode/huisnummer combinatie niet gevonden."
			goto sendResponse
		}
		locationLookup, err := pdok.LookupID(locationSuggest.Response.Docs[0].ID)
		if locationLookup.Response.NumFound == 0 {
			responseData.Error = "Postcode/huisnummer combinatie niet gevonden."
			goto sendResponse
		}
		lon, lat, err := pdok.DecodePoint(locationLookup.Response.Docs[0].CentroideLl)
		if err != nil {
			fmt.Printf("error looking up centerpoint: %v\n", err)
			http.Error(w, "failed to lookup centerpoint", http.StatusInternalServerError)
			return
		}
		responseData.CenterLatitude = lat
		responseData.CenterLongitude = lon

		searchResult, err := a.openingstijdenClient.Search(lat, lon)
		if err != nil {
			fmt.Printf("error finding shops: %v\n", err)
			http.Error(w, "failed to find shops", http.StatusInternalServerError)
			return
		}
		for _, place := range searchResult {
			responseData.Places = append(responseData.Places, PlacesResponse_Place{
				ID:         strconv.FormatInt(place.ID, 10),
				Name:       place.Name,
				Streetname: place.Street,
				Latitude:   place.Lat,
				Longtitude: place.Lon,
			})
		}
	}

sendResponse:
	err = json.NewEncoder(w).Encode(&responseData)
	if err != nil {
		fmt.Printf("error encoding response: %v", err)
		http.Error(w, "failed to encode json", http.StatusInternalServerError)
		return
	}
}

type MomentRequest struct {
	Zipcode     string `json:"zipcode"`
	Housenumber string `json:"housenumber"`
	PlaceID     string `json:"place_id"`
}

type MomentResponse struct {
	Error             string                  `json:"error"`
	OpeningstijdenURL string                  `json:"openingstijden_url"`
	Moments           []MomentResponse_Moment `json:"moments"`
}
type MomentResponse_Moment struct {
	Datetime time.Time `json:"datetime"`
}

func (a *API) Moment(w http.ResponseWriter, r *http.Request) {
	setCors(w)
	if r.Method == "OPTIONS" {
		return
	}

	var requestData MomentRequest
	err := json.NewDecoder(r.Body).Decode(&requestData)
	r.Body.Close()
	if err != nil {
		http.Error(w, "failed to decode json", http.StatusBadRequest)
		return
	}

	if len(requestData.Housenumber) > 10 {
		http.Error(w, "invalid housenumber, too long", http.StatusBadRequest)
		return
	}
	if len(requestData.Zipcode) > 10 {
		http.Error(w, "invalid zipcode, too long", http.StatusBadRequest)
		return
	}
	zipcode := strings.ReplaceAll(requestData.Zipcode, " ", "")
	if len(zipcode) != 6 {
		http.Error(w, "invalid zipcode, too long", http.StatusBadRequest)
		return
	}

	placeID, err := strconv.ParseInt(requestData.PlaceID, 10, 64)
	if err != nil {
		http.Error(w, "failed to parse place id", http.StatusBadRequest)
		return
	}
	placeDetails, err := a.openingstijdenClient.Details(placeID)
	if err != nil {
		fmt.Printf("Failed to fetch place details: %v\n", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	var responseData = MomentResponse{
		OpeningstijdenURL: placeDetails.URL,
	}

	now := time.Now()
	for _, openingTimes := range placeDetails.Times {
		safeEndTime := openingTimes.EndTime.Add(safeShoppingDuration)
		if safeEndTime.Before(openingTimes.StartTime) {
			continue
		}
		if safeEndTime.Before(now) {
			continue
		}

		momentDatetime := Hash(openingTimes.StartTime, safeEndTime, zipcode, requestData.Housenumber)
		if momentDatetime.Before(now) {
			continue
		}
		responseData.Moments = append(responseData.Moments, MomentResponse_Moment{Datetime: momentDatetime})
	}

	err = json.NewEncoder(w).Encode(&responseData)
	if err != nil {
		http.Error(w, "failed to encode json", http.StatusInternalServerError)
		fmt.Printf("error encoding response: %v\n", err)
		return
	}
}
