package momentjev2

import (
	"net/http"

	"gitlab.com/geenrij/momentje/openingstijden"
)

type API struct {
	openingstijdenClient *openingstijden.Client
}

func NewAPI(openingstijdenClient *openingstijden.Client) *API {
	return &API{
		openingstijdenClient: openingstijdenClient,
	}
}

func setCors(w http.ResponseWriter) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}
