package pdok

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

func SuggestZipcodeHousenumber(zipcode, housenumber string) (*SuggestData, error) {
	req := &http.Request{
		Method: "GET",
		URL: &url.URL{
			Scheme:   "https",
			Host:     lokatieserverHostname,
			Path:     "/locatieserver/v3/suggest",
			RawQuery: fmt.Sprintf("q=%s%%20%s", strings.TrimSpace(zipcode), strings.TrimSpace(housenumber)),
		},
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to get suggest: %w", err)
	}
	defer res.Body.Close()

	var data SuggestData
	err = json.NewDecoder(res.Body).Decode(&data)
	if err != nil {
		return nil, fmt.Errorf("failed to decode suggest response: %w", err)
	}
	return &data, nil
}

type SuggestData struct {
	Response struct {
		NumFound int     `json:"numFound"`
		Start    int     `json:"start"`
		MaxScore float64 `json:"maxScore"`
		Docs     []struct {
			Type         string  `json:"type"`
			Weergavenaam string  `json:"weergavenaam"`
			ID           string  `json:"id"`
			Score        float64 `json:"score"`
		} `json:"docs"`
	} `json:"response"`
}
