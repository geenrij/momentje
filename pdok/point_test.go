package pdok

import (
	"fmt"
	"testing"
)

func TestDecodePoint(t *testing.T) {
	lat, lon := 4.7457098, 52.6319281
	a, b, err := DecodePoint(fmt.Sprintf("POINT(%f %f)", lat, lon))
	if err != nil {
		t.Fatal(err)
	}
	if int(a*1000) != int(lat*1000) {
		t.Fatalf("incorrect decoded lat, expected %f, got %f", lat, a)
	}
	if int(b*1000) != int(lon*1000) {
		t.Fatalf("incorrect decoded lat, expected %f, got %f", lon, b)
	}
}
