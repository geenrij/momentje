// Code generated by ffjson <https://github.com/pquerna/ffjson>. DO NOT EDIT.
// source: pdok/lookup.go

package pdok

import (
	"bytes"
	"encoding/json"
	"fmt"
	fflib "github.com/pquerna/ffjson/fflib/v1"
)

// MarshalJSON marshal bytes to json - template
func (j *LookupData) MarshalJSON() ([]byte, error) {
	var buf fflib.Buffer
	if j == nil {
		buf.WriteString("null")
		return buf.Bytes(), nil
	}
	err := j.MarshalJSONBuf(&buf)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// MarshalJSONBuf marshal buff to json - template
func (j *LookupData) MarshalJSONBuf(buf fflib.EncodingBuffer) error {
	if j == nil {
		buf.WriteString("null")
		return nil
	}
	var err error
	var obj []byte
	_ = obj
	_ = err
	/* Inline struct. type=struct { NumFound int "json:\"numFound\""; Start int "json:\"start\""; MaxScore float64 "json:\"maxScore\""; Docs []struct { Bron string "json:\"bron\""; Woonplaatscode string "json:\"woonplaatscode\""; Type string "json:\"type\""; Woonplaatsnaam string "json:\"woonplaatsnaam\""; Wijkcode string "json:\"wijkcode\""; HuisNlt string "json:\"huis_nlt\""; Openbareruimtetype string "json:\"openbareruimtetype\""; Buurtnaam string "json:\"buurtnaam\""; Gemeentecode string "json:\"gemeentecode\""; RdfSeealso string "json:\"rdf_seealso\""; Weergavenaam string "json:\"weergavenaam\""; StraatnaamVerkort string "json:\"straatnaam_verkort\""; ID string "json:\"id\""; GekoppeldPerceel []string "json:\"gekoppeld_perceel\""; Gemeentenaam string "json:\"gemeentenaam\""; Buurtcode string "json:\"buurtcode\""; Wijknaam string "json:\"wijknaam\""; Identificatie string "json:\"identificatie\""; OpenbareruimteID string "json:\"openbareruimte_id\""; Waterschapsnaam string "json:\"waterschapsnaam\""; Provinciecode string "json:\"provinciecode\""; Postcode string "json:\"postcode\""; Provincienaam string "json:\"provincienaam\""; CentroideLl string "json:\"centroide_ll\""; NummeraanduidingID string "json:\"nummeraanduiding_id\""; Waterschapscode string "json:\"waterschapscode\""; AdresseerbaarobjectID string "json:\"adresseerbaarobject_id\""; Huisnummer int "json:\"huisnummer\""; Provincieafkorting string "json:\"provincieafkorting\""; CentroideRd string "json:\"centroide_rd\""; Straatnaam string "json:\"straatnaam\""; GekoppeldAppartement []string "json:\"gekoppeld_appartement\"" } "json:\"docs\"" } kind=struct */
	buf.WriteString(`{"response":{ "numFound":`)
	fflib.FormatBits2(buf, uint64(j.Response.NumFound), 10, j.Response.NumFound < 0)
	buf.WriteString(`,"start":`)
	fflib.FormatBits2(buf, uint64(j.Response.Start), 10, j.Response.Start < 0)
	buf.WriteString(`,"maxScore":`)
	fflib.AppendFloat(buf, float64(j.Response.MaxScore), 'g', -1, 64)
	buf.WriteString(`,"docs":`)
	if j.Response.Docs != nil {
		buf.WriteString(`[`)
		for i, v := range j.Response.Docs {
			if i != 0 {
				buf.WriteString(`,`)
			}
			/* Inline struct. type=struct { Bron string "json:\"bron\""; Woonplaatscode string "json:\"woonplaatscode\""; Type string "json:\"type\""; Woonplaatsnaam string "json:\"woonplaatsnaam\""; Wijkcode string "json:\"wijkcode\""; HuisNlt string "json:\"huis_nlt\""; Openbareruimtetype string "json:\"openbareruimtetype\""; Buurtnaam string "json:\"buurtnaam\""; Gemeentecode string "json:\"gemeentecode\""; RdfSeealso string "json:\"rdf_seealso\""; Weergavenaam string "json:\"weergavenaam\""; StraatnaamVerkort string "json:\"straatnaam_verkort\""; ID string "json:\"id\""; GekoppeldPerceel []string "json:\"gekoppeld_perceel\""; Gemeentenaam string "json:\"gemeentenaam\""; Buurtcode string "json:\"buurtcode\""; Wijknaam string "json:\"wijknaam\""; Identificatie string "json:\"identificatie\""; OpenbareruimteID string "json:\"openbareruimte_id\""; Waterschapsnaam string "json:\"waterschapsnaam\""; Provinciecode string "json:\"provinciecode\""; Postcode string "json:\"postcode\""; Provincienaam string "json:\"provincienaam\""; CentroideLl string "json:\"centroide_ll\""; NummeraanduidingID string "json:\"nummeraanduiding_id\""; Waterschapscode string "json:\"waterschapscode\""; AdresseerbaarobjectID string "json:\"adresseerbaarobject_id\""; Huisnummer int "json:\"huisnummer\""; Provincieafkorting string "json:\"provincieafkorting\""; CentroideRd string "json:\"centroide_rd\""; Straatnaam string "json:\"straatnaam\""; GekoppeldAppartement []string "json:\"gekoppeld_appartement\"" } kind=struct */
			buf.WriteString(`{ "bron":`)
			fflib.WriteJsonString(buf, string(v.Bron))
			buf.WriteString(`,"woonplaatscode":`)
			fflib.WriteJsonString(buf, string(v.Woonplaatscode))
			buf.WriteString(`,"type":`)
			fflib.WriteJsonString(buf, string(v.Type))
			buf.WriteString(`,"woonplaatsnaam":`)
			fflib.WriteJsonString(buf, string(v.Woonplaatsnaam))
			buf.WriteString(`,"wijkcode":`)
			fflib.WriteJsonString(buf, string(v.Wijkcode))
			buf.WriteString(`,"huis_nlt":`)
			fflib.WriteJsonString(buf, string(v.HuisNlt))
			buf.WriteString(`,"openbareruimtetype":`)
			fflib.WriteJsonString(buf, string(v.Openbareruimtetype))
			buf.WriteString(`,"buurtnaam":`)
			fflib.WriteJsonString(buf, string(v.Buurtnaam))
			buf.WriteString(`,"gemeentecode":`)
			fflib.WriteJsonString(buf, string(v.Gemeentecode))
			buf.WriteString(`,"rdf_seealso":`)
			fflib.WriteJsonString(buf, string(v.RdfSeealso))
			buf.WriteString(`,"weergavenaam":`)
			fflib.WriteJsonString(buf, string(v.Weergavenaam))
			buf.WriteString(`,"straatnaam_verkort":`)
			fflib.WriteJsonString(buf, string(v.StraatnaamVerkort))
			buf.WriteString(`,"id":`)
			fflib.WriteJsonString(buf, string(v.ID))
			buf.WriteString(`,"gekoppeld_perceel":`)
			if v.GekoppeldPerceel != nil {
				buf.WriteString(`[`)
				for i, v := range v.GekoppeldPerceel {
					if i != 0 {
						buf.WriteString(`,`)
					}
					fflib.WriteJsonString(buf, string(v))
				}
				buf.WriteString(`]`)
			} else {
				buf.WriteString(`null`)
			}
			buf.WriteString(`,"gemeentenaam":`)
			fflib.WriteJsonString(buf, string(v.Gemeentenaam))
			buf.WriteString(`,"buurtcode":`)
			fflib.WriteJsonString(buf, string(v.Buurtcode))
			buf.WriteString(`,"wijknaam":`)
			fflib.WriteJsonString(buf, string(v.Wijknaam))
			buf.WriteString(`,"identificatie":`)
			fflib.WriteJsonString(buf, string(v.Identificatie))
			buf.WriteString(`,"openbareruimte_id":`)
			fflib.WriteJsonString(buf, string(v.OpenbareruimteID))
			buf.WriteString(`,"waterschapsnaam":`)
			fflib.WriteJsonString(buf, string(v.Waterschapsnaam))
			buf.WriteString(`,"provinciecode":`)
			fflib.WriteJsonString(buf, string(v.Provinciecode))
			buf.WriteString(`,"postcode":`)
			fflib.WriteJsonString(buf, string(v.Postcode))
			buf.WriteString(`,"provincienaam":`)
			fflib.WriteJsonString(buf, string(v.Provincienaam))
			buf.WriteString(`,"centroide_ll":`)
			fflib.WriteJsonString(buf, string(v.CentroideLl))
			buf.WriteString(`,"nummeraanduiding_id":`)
			fflib.WriteJsonString(buf, string(v.NummeraanduidingID))
			buf.WriteString(`,"waterschapscode":`)
			fflib.WriteJsonString(buf, string(v.Waterschapscode))
			buf.WriteString(`,"adresseerbaarobject_id":`)
			fflib.WriteJsonString(buf, string(v.AdresseerbaarobjectID))
			buf.WriteString(`,"huisnummer":`)
			fflib.FormatBits2(buf, uint64(v.Huisnummer), 10, v.Huisnummer < 0)
			buf.WriteString(`,"provincieafkorting":`)
			fflib.WriteJsonString(buf, string(v.Provincieafkorting))
			buf.WriteString(`,"centroide_rd":`)
			fflib.WriteJsonString(buf, string(v.CentroideRd))
			buf.WriteString(`,"straatnaam":`)
			fflib.WriteJsonString(buf, string(v.Straatnaam))
			buf.WriteString(`,"gekoppeld_appartement":`)
			if v.GekoppeldAppartement != nil {
				buf.WriteString(`[`)
				for i, v := range v.GekoppeldAppartement {
					if i != 0 {
						buf.WriteString(`,`)
					}
					fflib.WriteJsonString(buf, string(v))
				}
				buf.WriteString(`]`)
			} else {
				buf.WriteString(`null`)
			}
			buf.WriteByte('}')
		}
		buf.WriteString(`]`)
	} else {
		buf.WriteString(`null`)
	}
	buf.WriteByte('}')
	buf.WriteByte('}')
	return nil
}

const (
	ffjtLookupDatabase = iota
	ffjtLookupDatanosuchkey

	ffjtLookupDataResponse
)

var ffjKeyLookupDataResponse = []byte("response")

// UnmarshalJSON umarshall json - template of ffjson
func (j *LookupData) UnmarshalJSON(input []byte) error {
	fs := fflib.NewFFLexer(input)
	return j.UnmarshalJSONFFLexer(fs, fflib.FFParse_map_start)
}

// UnmarshalJSONFFLexer fast json unmarshall - template ffjson
func (j *LookupData) UnmarshalJSONFFLexer(fs *fflib.FFLexer, state fflib.FFParseState) error {
	var err error
	currentKey := ffjtLookupDatabase
	_ = currentKey
	tok := fflib.FFTok_init
	wantedTok := fflib.FFTok_init

mainparse:
	for {
		tok = fs.Scan()
		//	println(fmt.Sprintf("debug: tok: %v  state: %v", tok, state))
		if tok == fflib.FFTok_error {
			goto tokerror
		}

		switch state {

		case fflib.FFParse_map_start:
			if tok != fflib.FFTok_left_bracket {
				wantedTok = fflib.FFTok_left_bracket
				goto wrongtokenerror
			}
			state = fflib.FFParse_want_key
			continue

		case fflib.FFParse_after_value:
			if tok == fflib.FFTok_comma {
				state = fflib.FFParse_want_key
			} else if tok == fflib.FFTok_right_bracket {
				goto done
			} else {
				wantedTok = fflib.FFTok_comma
				goto wrongtokenerror
			}

		case fflib.FFParse_want_key:
			// json {} ended. goto exit. woo.
			if tok == fflib.FFTok_right_bracket {
				goto done
			}
			if tok != fflib.FFTok_string {
				wantedTok = fflib.FFTok_string
				goto wrongtokenerror
			}

			kn := fs.Output.Bytes()
			if len(kn) <= 0 {
				// "" case. hrm.
				currentKey = ffjtLookupDatanosuchkey
				state = fflib.FFParse_want_colon
				goto mainparse
			} else {
				switch kn[0] {

				case 'r':

					if bytes.Equal(ffjKeyLookupDataResponse, kn) {
						currentKey = ffjtLookupDataResponse
						state = fflib.FFParse_want_colon
						goto mainparse
					}

				}

				if fflib.EqualFoldRight(ffjKeyLookupDataResponse, kn) {
					currentKey = ffjtLookupDataResponse
					state = fflib.FFParse_want_colon
					goto mainparse
				}

				currentKey = ffjtLookupDatanosuchkey
				state = fflib.FFParse_want_colon
				goto mainparse
			}

		case fflib.FFParse_want_colon:
			if tok != fflib.FFTok_colon {
				wantedTok = fflib.FFTok_colon
				goto wrongtokenerror
			}
			state = fflib.FFParse_want_value
			continue
		case fflib.FFParse_want_value:

			if tok == fflib.FFTok_left_brace || tok == fflib.FFTok_left_bracket || tok == fflib.FFTok_integer || tok == fflib.FFTok_double || tok == fflib.FFTok_string || tok == fflib.FFTok_bool || tok == fflib.FFTok_null {
				switch currentKey {

				case ffjtLookupDataResponse:
					goto handle_Response

				case ffjtLookupDatanosuchkey:
					err = fs.SkipField(tok)
					if err != nil {
						return fs.WrapErr(err)
					}
					state = fflib.FFParse_after_value
					goto mainparse
				}
			} else {
				goto wantedvalue
			}
		}
	}

handle_Response:

	/* handler: j.Response type=struct { NumFound int "json:\"numFound\""; Start int "json:\"start\""; MaxScore float64 "json:\"maxScore\""; Docs []struct { Bron string "json:\"bron\""; Woonplaatscode string "json:\"woonplaatscode\""; Type string "json:\"type\""; Woonplaatsnaam string "json:\"woonplaatsnaam\""; Wijkcode string "json:\"wijkcode\""; HuisNlt string "json:\"huis_nlt\""; Openbareruimtetype string "json:\"openbareruimtetype\""; Buurtnaam string "json:\"buurtnaam\""; Gemeentecode string "json:\"gemeentecode\""; RdfSeealso string "json:\"rdf_seealso\""; Weergavenaam string "json:\"weergavenaam\""; StraatnaamVerkort string "json:\"straatnaam_verkort\""; ID string "json:\"id\""; GekoppeldPerceel []string "json:\"gekoppeld_perceel\""; Gemeentenaam string "json:\"gemeentenaam\""; Buurtcode string "json:\"buurtcode\""; Wijknaam string "json:\"wijknaam\""; Identificatie string "json:\"identificatie\""; OpenbareruimteID string "json:\"openbareruimte_id\""; Waterschapsnaam string "json:\"waterschapsnaam\""; Provinciecode string "json:\"provinciecode\""; Postcode string "json:\"postcode\""; Provincienaam string "json:\"provincienaam\""; CentroideLl string "json:\"centroide_ll\""; NummeraanduidingID string "json:\"nummeraanduiding_id\""; Waterschapscode string "json:\"waterschapscode\""; AdresseerbaarobjectID string "json:\"adresseerbaarobject_id\""; Huisnummer int "json:\"huisnummer\""; Provincieafkorting string "json:\"provincieafkorting\""; CentroideRd string "json:\"centroide_rd\""; Straatnaam string "json:\"straatnaam\""; GekoppeldAppartement []string "json:\"gekoppeld_appartement\"" } "json:\"docs\"" } kind=struct quoted=false*/

	{
		/* Falling back. type=struct { NumFound int "json:\"numFound\""; Start int "json:\"start\""; MaxScore float64 "json:\"maxScore\""; Docs []struct { Bron string "json:\"bron\""; Woonplaatscode string "json:\"woonplaatscode\""; Type string "json:\"type\""; Woonplaatsnaam string "json:\"woonplaatsnaam\""; Wijkcode string "json:\"wijkcode\""; HuisNlt string "json:\"huis_nlt\""; Openbareruimtetype string "json:\"openbareruimtetype\""; Buurtnaam string "json:\"buurtnaam\""; Gemeentecode string "json:\"gemeentecode\""; RdfSeealso string "json:\"rdf_seealso\""; Weergavenaam string "json:\"weergavenaam\""; StraatnaamVerkort string "json:\"straatnaam_verkort\""; ID string "json:\"id\""; GekoppeldPerceel []string "json:\"gekoppeld_perceel\""; Gemeentenaam string "json:\"gemeentenaam\""; Buurtcode string "json:\"buurtcode\""; Wijknaam string "json:\"wijknaam\""; Identificatie string "json:\"identificatie\""; OpenbareruimteID string "json:\"openbareruimte_id\""; Waterschapsnaam string "json:\"waterschapsnaam\""; Provinciecode string "json:\"provinciecode\""; Postcode string "json:\"postcode\""; Provincienaam string "json:\"provincienaam\""; CentroideLl string "json:\"centroide_ll\""; NummeraanduidingID string "json:\"nummeraanduiding_id\""; Waterschapscode string "json:\"waterschapscode\""; AdresseerbaarobjectID string "json:\"adresseerbaarobject_id\""; Huisnummer int "json:\"huisnummer\""; Provincieafkorting string "json:\"provincieafkorting\""; CentroideRd string "json:\"centroide_rd\""; Straatnaam string "json:\"straatnaam\""; GekoppeldAppartement []string "json:\"gekoppeld_appartement\"" } "json:\"docs\"" } kind=struct */
		tbuf, err := fs.CaptureField(tok)
		if err != nil {
			return fs.WrapErr(err)
		}

		err = json.Unmarshal(tbuf, &j.Response)
		if err != nil {
			return fs.WrapErr(err)
		}
	}

	state = fflib.FFParse_after_value
	goto mainparse

wantedvalue:
	return fs.WrapErr(fmt.Errorf("wanted value token, but got token: %v", tok))
wrongtokenerror:
	return fs.WrapErr(fmt.Errorf("ffjson: wanted token: %v, but got token: %v output=%s", wantedTok, tok, fs.Output.String()))
tokerror:
	if fs.BigError != nil {
		return fs.WrapErr(fs.BigError)
	}
	err = fs.Error.ToError()
	if err != nil {
		return fs.WrapErr(err)
	}
	panic("ffjson-generated: unreachable, please report bug.")
done:

	return nil
}
