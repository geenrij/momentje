package pdok

import (
	"errors"
	"strconv"
	"strings"
)

var ErrInvalidPoint = errors.New("error decoding point")

func DecodePoint(point string) (a, b float64, err error) {
	const prefix = "POINT("
	const suffix = ")"

	if point[0:len(prefix)] != prefix {
		return 0, 0, ErrInvalidPoint
	}
	if point[len(point)-len(suffix):] != suffix {
		return 0, 0, ErrInvalidPoint
	}
	point = point[len(prefix) : len(point)-len(suffix)]

	parts := strings.Split(point, " ")
	if len(parts) != 2 {
		return 0, 0, ErrInvalidPoint
	}

	a, err = strconv.ParseFloat(parts[0], 64)
	if err != nil {
		return 0, 0, ErrInvalidPoint
	}
	b, err = strconv.ParseFloat(parts[1], 64)
	if err != nil {
		return 0, 0, ErrInvalidPoint
	}

	return a, b, nil
}
