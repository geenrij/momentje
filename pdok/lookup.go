package pdok

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

func LookupID(id string) (*LookupData, error) {
	req := &http.Request{
		Method: "GET",
		URL: &url.URL{
			Scheme:   "https",
			Host:     lokatieserverHostname,
			Path:     "/locatieserver/v3/lookup",
			RawQuery: fmt.Sprintf("id=%s", id),
		},
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to get suggest: %w", err)
	}
	defer res.Body.Close()

	var data LookupData
	err = json.NewDecoder(res.Body).Decode(&data)
	if err != nil {
		return nil, fmt.Errorf("failed to decode suggest response: %w", err)
	}
	return &data, nil
}

type LookupData struct {
	Response struct {
		NumFound int     `json:"numFound"`
		Start    int     `json:"start"`
		MaxScore float64 `json:"maxScore"`
		Docs     []struct {
			Bron                  string   `json:"bron"`
			Woonplaatscode        string   `json:"woonplaatscode"`
			Type                  string   `json:"type"`
			Woonplaatsnaam        string   `json:"woonplaatsnaam"`
			Wijkcode              string   `json:"wijkcode"`
			HuisNlt               string   `json:"huis_nlt"`
			Openbareruimtetype    string   `json:"openbareruimtetype"`
			Buurtnaam             string   `json:"buurtnaam"`
			Gemeentecode          string   `json:"gemeentecode"`
			RdfSeealso            string   `json:"rdf_seealso"`
			Weergavenaam          string   `json:"weergavenaam"`
			StraatnaamVerkort     string   `json:"straatnaam_verkort"`
			ID                    string   `json:"id"`
			GekoppeldPerceel      []string `json:"gekoppeld_perceel"`
			Gemeentenaam          string   `json:"gemeentenaam"`
			Buurtcode             string   `json:"buurtcode"`
			Wijknaam              string   `json:"wijknaam"`
			Identificatie         string   `json:"identificatie"`
			OpenbareruimteID      string   `json:"openbareruimte_id"`
			Waterschapsnaam       string   `json:"waterschapsnaam"`
			Provinciecode         string   `json:"provinciecode"`
			Postcode              string   `json:"postcode"`
			Provincienaam         string   `json:"provincienaam"`
			CentroideLl           string   `json:"centroide_ll"`
			NummeraanduidingID    string   `json:"nummeraanduiding_id"`
			Waterschapscode       string   `json:"waterschapscode"`
			AdresseerbaarobjectID string   `json:"adresseerbaarobject_id"`
			Huisnummer            int      `json:"huisnummer"`
			Provincieafkorting    string   `json:"provincieafkorting"`
			CentroideRd           string   `json:"centroide_rd"`
			Straatnaam            string   `json:"straatnaam"`
			GekoppeldAppartement  []string `json:"gekoppeld_appartement"`
		} `json:"docs"`
	} `json:"response"`
}
