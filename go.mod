module gitlab.com/geenrij/momentje

go 1.14

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/dgryski/go-jump v0.0.0-20170409065014-e1f439676b57
	github.com/pquerna/ffjson v0.0.0-20190930134022-aa0246cd15f7
)
