# momentje - API service for geenrij.nu

This repository contains the open-source code for the geenrij.nu backend services.

The current API uses some external services and libraries:

- PDOK locatieserver, to get a latitude and longitude for a given postal code and housenumber.
- Openingstijden.nl API, to get shops near a latitude/longitude, and their opening times.
- [CRC32](https://golang.org/pkg/hash/crc32/) and the [Jump](github.com/dgryski/go-jump) packages, to hash the unique data (day, zipcode, housenumber) over a list of moments during opening times of a store.

## Develop

- Run [modd](https://github.com/cortesi/modd) for development building/restarting.
- The project uses [ffjson](https://github.com/pquerna/ffjson) for extremely fast json encoding/decoding, make sure to regenerate ffjson-generated code after modifying data structures in the api packages, do this by deleting the _ffjson.go file and running ffjson again (see modd.conf for run example).
- Deployed using `deploy.sh`.
