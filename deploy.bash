#!/bin/bash

defaultEnv="staging"
echo -n "Which environment to deploy to? (${defaultEnv}): "
read -r env
if [ -z "$env" ]
then
	env=$defaultEnv
fi

set -euxo pipefail

export CLOUDSDK_CORE_PROJECT=s11-geenrij-${env}

gcloud builds submit --tag gcr.io/s11-geenrij-${env}/momentje

gcloud run deploy momentje --image gcr.io/s11-geenrij-${env}/momentje --platform managed --region europe-west4
